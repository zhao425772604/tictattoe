<?php
include($_SERVER['DOCUMENT_ROOT']."/dbms.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  $content=trim(file_get_contents("php://input"));
  $decoded=json_decode($content,true);
  $gameID = $decoded['id'];
  $m=new MongoDB\Driver\Manager();
  $query=new MongoDB\Driver\Query([]);
  session_start();
  // $username = $_SESSION['username'];
  $rows=$m->executeQuery("wp2.gameHistory",$query);


  foreach($rows as $row){
    if ($row->gameID== $gameID){
      $games_array = $row->grid;
      $winner = $row->winner;
      $returndata =array();
      $returndata['status']="OK";
      $returndata['grid'] = $games_array;
      $returndata['winner'] = $winner;
      echo $_POST['callback'].json_encode($returndata);

    }
  }
  
}


?>
