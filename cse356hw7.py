
import pymysql
pymysql.install_as_MySQLdb()

import pymongo

import string
import random

def mysql_find(club,position):
    # db=pymysql.connect(host="130.245.168.85",user="root",passwd="123456",db="mysql_db3",charset='utf8')
    # cursor=db.cursor()
    # if style=="username":
    #     cursor.execute("select * from users where username=%s",item)
    # elif style=="userid":
    #     cursor.execute("select * from users where userid=%s",item)
    # elif style=="email":
    #     cursor.execute("select * from users where email=%s",item)
    # elif style=="verify":
    #     cursor.execute("select * from verifytable where email=%s",item)
    #
    # rs=cursor.fetchone()
    # cursor.close()
    # db.close()
    # return rs   #control excetion later
    db=pymysql.connect(host="130.245.168.85",user="root",passwd="123456",db="hw7",charset='utf8')
    cursor=db.cursor()
    rows_count = cursor.execute("select * from assists where Club=%s AND POS=%s",[club,position])
    if rows_count > 0:
        rs=cursor.fetchall()
        max_assist = 0
        player = None
        avg_assist = 0.0
        i = 0
        assist_total = 0
        max_gs_score = 0
        while i < rows_count:
            if rs[i][6] > max_assist:
                max_assist = rs[i][6]
                player = rs[i][1]
                max_gs_score = rs[i][5]
            if rs[i][6] == max_assist:
                if rs[i][5] > max_gs_score:
                    player = rs[i][1]
                    max_gs_score = rs[i][5]
            assist_total += rs[i][6]
            i += 1
        avg_assist = assist_total / rows_count
        r = []
        r.append({"pos": position, "club" : club,"max_assist": max_assist,"player": player, "avg_assist": avg_assist,"max_gs_score" : max_gs_score})
        cursor.close()
        db.close()
        return r
    else:
        cursor.close()
        db.close()
        return None

    #return rs   #control excetion later
res = mysql_find("SKC", "F")
print(res[0]["player"])
