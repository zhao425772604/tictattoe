
<?php
//include 'dbms.php';
include($_SERVER['DOCUMENT_ROOT']."/dbms.php");
if ($_SERVER['REQUEST_METHOD'] == 'GET'){
 // include('ok.html');
    session_start();
    if(isset($_SESSION['id']) && !empty($_SESSION['id'])){
      $board =array();
      for($i=0;$i<9;$i++){
      	array_push($board, ' ');
      }
      db_createGame();
      //createGame();
      // $board = array('','','','','','','','','');
      $_SESSION['board'] = $board;
      echo '
      <!DOCTYPE html>
      <html>
      <head>
        <title>Tic-Tac-Toe | Welcome</title>
        <link href="../css/main.css" type="text/css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
        <script src="../js/main.js"></script>
      </head><body>
        <div class="welcome-message">Hello ' . $_SESSION['username'] . ', ' . date("Y-m-d")  .'
        </div>
      <div class="grid center">
        <div class="column"  id="c0" onclick="fromttt(this,0)"></div>
        <div class="column"  id="c1" onclick="fromttt(this,1)"></div>
        <div class="column"  id="c2" onclick="fromttt(this,2)"></div>
        <div class="column"  id="c3" onclick="fromttt(this,3)"></div>
        <div class="column"  id="c4" onclick="fromttt(this,4)"></div>
        <div class="column"  id="c5" onclick="fromttt(this,5)"></div>
        <div class="column"  id="c6" onclick="fromttt(this,6)"></div>
        <div class="column"  id="c7" onclick="fromttt(this,7)"></div>
        <div class="column"  id="c8" onclick="fromttt(this,8)"></div>
      </div>
      <div id="bottomLinks" class="grid center" style="margin-top:3%; margin-left: 10%;">
          <form action = "../listgames" method="post">
            <input type="submit" value="List games">
          </form>
          <form action = "../getgame" method="post">
            <label>Game ID </label>
            <input type="number" name="gameID">
            <input type="submit" value="Get Game">
          </form>
          <form action = "../getscore" method="post">
            <input type="submit" value="Get Score">
          </form>
          <form action = "../logout" method = "post">
            <input type="submit" value ="Log out">
          </form>
      </div>
      </body>
      </html>
  ';
    }
    else{
       echo '
       <!DOCTYPE html>
       <html>

       <head>
        <title>Tic-Tac-Toe | Welcome</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="../js/main.js"></script>
        <script src="../js/home.js"></script>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link href="../css/main.css" type="text/css" rel="stylesheet">
       </head>

       <body>
         <div class="container-fluid" style="margin-top:5%;">
             <div class="row">
               <div class="col-sm-4 col-md-4"></div>
               <div class="col-sm-4 col-md-4">
                 <div id = "loginFormDiv" class="well">
                   <br>
                   <form onsubmit="login(this)">
                     <div class="w3-section">
                       <label>Username:</label>
                       <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="username" name="username" required>
                       <label>Password:</label>
                       <input class="w3-input w3-border" type="password" placeholder="Password" name="password" required>
                       <center>
                         <button class="btn formButton" id ="loginButton" type="submit" style="margin-top:10%;">LOG IN</button>
                       </center>
                     </div>
                   </form>
                 </div>
                 <div id = "signUpFormDiv" class="well" style="display:none;">
                   <br>
                    <form onsubmit="signUp(this)">

                     <div class="w3-section">
                       <label>Username:</label>
                       <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="username" name="username"  required>
                       <label>Password:</label>
                       <input class="w3-input w3-border" type="password" placeholder="password" name="password"  required>
                       <label>Email:</label>
                       <input class="w3-input w3-border" type="email" placeholder="email" name="email"  required>
                       <center>
                         <button class="btn formButton" type="submit" style="margin-top:10%;">SIGN UP</button>
                       </center>
                     </div>
                   </form>
                 </div>
                 <div id = "verifyFormDiv" class="well" style="display:none;">
                   <br>
                   <form onsubmit="verify(this)">
                     <div class="w3-section">
                       <label>Email:</label>
                       <input class="w3-input w3-border" type="email" placeholder="email" name="email"  required>
                       <label>Key:</label>
                       <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="key" name="key"  required>
                       <center>
                         <button class="btn formButton" type="submit" style="margin-top:10%;">VERIFY</button>
                       </center>
                     </div>
                   </form>
                 </div>
                 <center>
                   <div id="bottomLinks" style="margin-top:3%;">
                       <button class="btn formButton" onclick = "displayLoginForm()" style="color:black;">Login</button>
                       <button class="btn formButton" onclick = "displaySignUpForm()" style="color:black;">Sign Up</button>
                       <button class="btn formButton" onclick = "displayVerifyForm()" style="color:black;">Verify Account</button>
                   </div>
                 </center>
               </div>
             </div>
           </div>

       </body>

       </html>
       ';
    }
}else if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  if($_POST['name']){
    echo '
    <!DOCTYPE html>
    <html>
    <head>
      <title>Tic-Tac-Toe | Welcome</title>
      <link href="../css/main.css" type="text/css" rel="stylesheet">
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
      <script src="../js/main.js"></script>
    </head><body>
      <div class="welcome-message">Hello ' . $_POST['name'] . ', ' . date("Y-m-d")  .'
      </div>
    <div class="grid center">
      <div class="column"  id="c0" onclick="fromttt(this,0)"></div>
      <div class="column"  id="c1" onclick="fromttt(this,1)"></div>
      <div class="column"  id="c2" onclick="fromttt(this,2)"></div>
      <div class="column"  id="c3" onclick="fromttt(this,3)"></div>
      <div class="column"  id="c4" onclick="fromttt(this,4)"></div>
      <div class="column"  id="c5" onclick="fromttt(this,5)"></div>
      <div class="column"  id="c6" onclick="fromttt(this,6)"></div>
      <div class="column"  id="c7" onclick="fromttt(this,7)"></div>
      <div class="column"  id="c8" onclick="fromttt(this,8)"></div>
    </div>
    </body>
    </html>
';

  }



}

?>
